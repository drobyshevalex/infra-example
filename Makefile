.PHONY: docs
docs:
	- docker compose -f ./docs/src/docker-compose.yaml up

.PHONY: docs-build
docs-build:
	- docker compose -f ./docs/src/docker-compose.yaml up --build
