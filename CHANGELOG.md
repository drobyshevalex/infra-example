## 1.0.14 (2024-08-21)

### feature (1 change)

- [Merge branch 'feature/8' into 'main'](https://gitlab.com/drobyshevalex/infra-example/-/commit/2c20f95fce5e516a2b37dba6f84866b9f8893dbb) ([merge request](https://gitlab.com/drobyshevalex/infra-example/-/merge_requests/30))

## 1.0.13 (2024-08-21)

No changes.

## 1.0.12 (2024-08-21)

### feature (1 change)

- [fix](https://gitlab.com/drobyshevalex/infra-example/-/commit/ef4cc5b8c1a5b4e812fa1f7f9578f49636507d12) ([merge request](https://gitlab.com/drobyshevalex/infra-example/-/merge_requests/28))

## 1.0.11 (2024-08-21)

No changes.

## 1.0.10 (2024-08-21)

No changes.

## 1.0.9 (2024-08-21)

No changes.

## 1.0.8 (2024-06-30)

### feature (1 change)

- [Merge branch 'feat/git' into 'main'](https://gitlab.com/drobyshevalex/infra-example/-/commit/b8abbeb126c73536351e97e67c92269ee7bfa83a) ([merge request](https://gitlab.com/drobyshevalex/infra-example/-/merge_requests/21))

## 1.0.7 (2024-06-30)

No changes.

## 1.0.6 (2024-06-30)

### feature (3 changes)

- [added db](https://gitlab.com/drobyshevalex/infra-example/-/commit/908bab3e8121fecd25b86c8491ec29239eeea7a3) ([merge request](https://gitlab.com/drobyshevalex/infra-example/-/merge_requests/19))
- [ecample env file](https://gitlab.com/drobyshevalex/infra-example/-/commit/df82b71a87a34dbd374b8627c395942c97089268) ([merge request](https://gitlab.com/drobyshevalex/infra-example/-/merge_requests/19))
- [added env example file](https://gitlab.com/drobyshevalex/infra-example/-/commit/2e2329917e75a58c2650c742363252a2e2acbf46) ([merge request](https://gitlab.com/drobyshevalex/infra-example/-/merge_requests/19))

### bug (1 change)

- [test 2](https://gitlab.com/drobyshevalex/infra-example/-/commit/937370e84685d72342ce6d327928ad7be1dce996) ([merge request](https://gitlab.com/drobyshevalex/infra-example/-/merge_requests/19))

## 1.0.4 (2024-06-30)

No changes.

## 1.0.3 (2024-06-30)

### feature (1 change)

- [added node mosules](https://gitlab.com/drobyshevalex/infra-example/-/commit/7596ad1e00658ddd56ad9eb120c13ffa9e280de8) ([merge request](https://gitlab.com/drobyshevalex/infra-example/-/merge_requests/16))

## 1.0.2 (2024-06-30)

No changes.

## 1.0.1 (2024-06-30)

No changes.
