const exec = require("child_process").exec;

export default () => {
  console.log("TEST PLUGIN LOADED");

  exec(
    `structurizr-cli export -workspace /opt/pages/sz.dsl -format mermaid -output /opt/pages/diagrams`,
    (error, stdout, stderr) => {
      console.log(error);
      console.log(stderr);
      console.log(stdout);
    }
  );

  return {
    name: "TEST",
    loadContent: async () => {
      console.log("TEST PLUGIN loadContent");
    },
  };
};
