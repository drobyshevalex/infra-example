import { visit } from "unist-util-visit";
import nodeFetch from "node-fetch";

export default (options) => {
  return async (tree) => {
    const temp = [];
    visit(tree, "code", (node) => {
      temp.push(trans({ node }));
    });

    for await (const _ of temp) {
    }
  };
};

const exec = require("child_process").exec;

async function conv({ type, value }) {
  return new Promise((resolve, reject) => {
    exec(
      `echo "${Buffer.from(value).toString(
        "base64"
      )}" | base64 -d | ./bin/kroki convert - --type ${type}`,
      (error, stdout, stderr) => {
        console.log(error);
        console.log(stderr);
        resolve(Buffer.from(stdout).toString("base64"));
      }
    );
  });
}

async function trans({ node }) {
  //console.log(node);

  // const data = await Fetch({
  //   server: process.env.KROKI_HOST,
  //   type: node.lang,
  //   value: node.value,
  // });

  const data = await conv({
    type: node.lang,
    value: node.value,
  });

  Object.assign(node, {
    type: "paragraph",
    children: [
      {
        type: "mdxJsxTextElement",
        name: "img",
        attributes: [
          {
            type: "mdxJsxAttribute",
            name: "className",
            value: "kroki-image",
          },
          {
            type: "mdxJsxAttribute",
            name: "alt",
            value: node.lang,
          },
          {
            type: "mdxJsxAttribute",
            name: "data-type",
            value: node.lang,
          },
          {
            type: "mdxJsxAttribute",
            name: "src",
            value: toDataURL(data),
          },
        ],
      },
    ],
  });
}

const mime = "image/svg+xml";

function toDataURL(buffer) {
  const base64 = buffer.toString("base64");

  const d = `data:${mime};base64,${base64}`;
  return d;
}

function Fetch({ server, headers, type, value }) {
  const serverURL = server.replace(/\/$/, "");

  return httpPost({
    url: `${serverURL}/${type}/svg`,
    body: value,
    headers,
  })
    .catch((error) => console.log(error))
    .then((data) => Buffer.from(data));
}

function httpPost({ url, body, headers }) {
  return nodeFetch(url, {
    method: "POST",
    body,
    headers: {
      ...headers,
      "Content-Type": "text/plain",
    },
  }).then(async (response) => {
    if (!response.ok) {
      throw new Error(await response.text());
    }

    return response.arrayBuffer();
  });
}
