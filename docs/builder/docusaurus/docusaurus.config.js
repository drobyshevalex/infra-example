// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import { themes as prismThemes, themes } from "prism-react-renderer";

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "My Site",
  tagline: "Dinosaurs are cool",
  //favicon: "img/favicon.ico",

  // Set the production url of your site here
  url: "https://your-docusaurus-site.example.com",
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: "/",

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: "facebook", // Usually your GitHub org/user name.
  projectName: "docusaurus", // Usually your repo name.

  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: "en",
    locales: ["en"],
  },

  presets: [
    [
      "classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          path: "/opt/pages",
          // path: "../../pages",
          sidebarPath: "./sidebars.js",
          routeBasePath: "/",
          remarkPlugins: [require("./src/plugins/kroki")],
        },
        theme: {
          customCss: "/opt/css/style.css",
          // customCss: "../../css/style.css",
        },
      }),
    ],
  ],

  plugins: [
    [
      "docusaurus-plugin-structurizr",
      // All options are optional
      // Default values are shown below
      {
        enabled: true,
        paths: ["/opt/pages"],
        format: "plantuml/c4plantuml", // "mermaid" | "plantuml" | <structurizr-cli format: https://docs.structurizr.com/cli/export>
        executor: "cli", // "docker" | "cli" | "auto",
        additionalStructurizrArgs: undefined, // string
        outputDir: undefined, // Generate all diagrams in a single directory. E.g. "diagrams".
        ignorePatterns: ["/**/include.*.dsl"], // automatically exclude import files (eg: !import ../common/import.actors.dsl)
      },
    ],
    // "./src/plugins/test",
  ],

  markdown: {
    mermaid: true,
  },

  themes: ["@docusaurus/theme-mermaid"],
};

export default config;
