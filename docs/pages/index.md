---
sidebar_position: 1
---

# Service

## Description

Docs example

## Examples

### Table

| id  | name |
| --- | ---- |
| 1   | foo  |
| 2   | bar  |

### structurizr

import szp from '!!raw-loader!./structurizr-SystemContext.puml'
import sz from '!!raw-loader!./structurizr-Containers.mmd'
import Mermaid from '@theme/Mermaid'

<Mermaid value={sz} />

```c4plantuml
@startuml
set separator none
title Software System - Containers

left to right direction

!include <C4/C4>
!include <C4/C4_Context>
!include <C4/C4_Container>

Person(User, "User", $descr="", $tags="", $link="")

System_Boundary("SoftwareSystem_boundary", "Software System", $tags="") {
  Container(SoftwareSystem.WebApplication, "Web Application", $techn="", $descr="", $tags="", $link="http://google.com")
  Container(SoftwareSystem.DatabaseSchema, "Database Schema", $techn="", $descr="", $tags="", $link="")
}

Rel(User, SoftwareSystem.WebApplication, "Uses", $techn="", $tags="", $link="")
Rel(SoftwareSystem.WebApplication, SoftwareSystem.DatabaseSchema, "Reads from and writes to", $techn="", $tags="", $link="")

SHOW_LEGEND(true)
@enduml
```

### mermaid

```mermaid
graph LR
  linkStyle default fill:#ffffff

  subgraph diagram ["Spring PetClinic - Deployment - Live"]
    style diagram fill:#ffffff,stroke:#ffffff

    subgraph 5 [Amazon Web Services]
      style 5 fill:#ffffff,stroke:#232f3e,color:#232f3e

      subgraph 6 [US-East-1]
        style 6 fill:#ffffff,stroke:#147eba,color:#147eba

        subgraph 12 [Amazon RDS]
          style 12 fill:#ffffff,stroke:#3b48cc,color:#3b48cc

          subgraph 13 [MySQL]
            style 13 fill:#ffffff,stroke:#3b48cc,color:#3b48cc

            14[("<div style='font-weight: bold'>Database</div><div style='font-size: 70%; margin-top: 0px'>[Container: Relational database schema]</div><div style='font-size: 80%; margin-top:10px'>Stores information regarding<br />the veterinarians, the<br />clients, and their pets.</div>")]
            style 14 fill:#ffffff,stroke:#b2b2b2,color:#000000
          end

        end

        7("<div style='font-weight: bold'>Route 53</div><div style='font-size: 70%; margin-top: 0px'>[Infrastructure Node]</div><div style='font-size: 80%; margin-top:10px'>Highly available and scalable<br />cloud DNS service.</div>")
        style 7 fill:#ffffff,stroke:#693cc5,color:#693cc5
        8("<div style='font-weight: bold'>Elastic Load Balancer</div><div style='font-size: 70%; margin-top: 0px'>[Infrastructure Node]</div><div style='font-size: 80%; margin-top:10px'>Automatically distributes<br />incoming application traffic.</div>")
        style 8 fill:#ffffff,stroke:#693cc5,color:#693cc5
        subgraph 9 [Autoscaling group]
          style 9 fill:#ffffff,stroke:#cc2264,color:#cc2264

          subgraph 10 [Amazon EC2]
            style 10 fill:#ffffff,stroke:#d86613,color:#d86613

            11("<div style='font-weight: bold'>Web Application</div><div style='font-size: 70%; margin-top: 0px'>[Container: Java and Spring Boot]</div><div style='font-size: 80%; margin-top:10px'>Allows employees to view and<br />manage information regarding<br />the veterinarians, the<br />clients, and their pets.</div>")
            style 11 fill:#ffffff,stroke:#b2b2b2,color:#000000
          end

        end

      end

    end

    11-. "<div>Reads from and writes to</div><div style='font-size: 70%'>[MySQL Protocol/SSL]</div>" .->14
    7-. "<div>Forwards requests to</div><div style='font-size: 70%'>[HTTPS]</div>" .->8
    8-. "<div>Forwards requests to</div><div style='font-size: 70%'>[HTTPS]</div>" .->11
  end
```

### UML

```plantuml
@startuml
Bob -> Alice : hello
@enduml
```

### graphviz

```graphviz
digraph G {Hello->World}
```

### blockdiag

```blockdiag
blockdiag {
  Kroki -> generates -> "Block diagrams";
  Kroki -> is -> "very easy!";

  Kroki [color = "greenyellow"];
  "Block diagrams" [color = "pink"];
  "very easy!" [color = "orange"];
}
```

### DBML

```dbml
Table users {
  id integer
  username varchar
  role varchar
  created_at timestamp
}

Table posts {
  id integer [primary key]
  title varchar
  body text [note: 'Content of the post']
  user_id integer
  status post_status
  created_at timestamp
}

Enum post_status {
  draft
  published
  private [note: 'visible via URL only']
}

Ref: posts.user_id > users.id // many-to-one
```

### c4plantuml

```c4plantuml
!include <C4/C4_Context>

title System Context diagram for Internet Banking System

Person(customer, "Banking Customer", "A customer of the bank, with personal bank accounts.")
System(banking_system, "Internet Banking System", "Allows customers to check their accounts.")

System_Ext(mail_system, "E-mail system", "The internal Microsoft Exchange e-mail system.")
System_Ext(mainframe, "Mainframe Banking System", "Stores all of the core banking information.")

Rel(customer, banking_system, "Uses")
Rel_Back(customer, mail_system, "Sends e-mails to")
Rel_Neighbor(banking_system, mail_system, "Sends e-mails", "SMTP")
Rel(banking_system, mainframe, "Uses")
```

### Plugin

import { Highlight } from './Highlight';
import MyComp from '@site/src/components/MyComp';

<Highlight color="#25c2a0">Docusaurus green</Highlight>
<MyComp color="#ff0000">Docusaurus red</MyComp>
