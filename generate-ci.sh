#!/bin/bash
echo "stages:
  - test" > .gitlab-ci.generated.yml

for svc in ms/*; do
  svc_name=$(basename $svc)
  echo "
test_${svc_name}:
  stage: test
  script:
    - echo \"Testing $svc_name\"
    - cd ms/$svc_name && go test ./...
  " >> .gitlab-ci.generated.yml
done
